Rails.application.routes.draw do
 namespace :api do
  resources :user, :expect => [:index]
  get 'users', to: 'user#create'
  get 'typeahead/:input', to: 'user#typeahead'
 end
end
